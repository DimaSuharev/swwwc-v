import Vue from "vue";
import "./myfont.font";

window.Vue = Vue;
import VueRouter from "vue-router";
Vue.use(VueRouter);

import VueAxios from "vue-axios";
import axios from "axios";
Vue.use(VueAxios, axios);

import Vuex from "vuex";
Vue.use(Vuex);

import Vue2TouchEvents from "vue2-touch-events";
Vue.use(Vue2TouchEvents);

import VueCookies from "vue-cookies";
Vue.use(VueCookies);

import GAService from "./views/services/GAService";
Vue.use(GAService);

import router from "./router";
import store from "./store";

import RightsService from "./views/services/RightsService";
Vue.use(RightsService, store);

import Main from "./views/Main.vue";

new Vue({
    el: "#app",
    store,
    router,
    components: {
        "main-app": Main
    },
    template: "<main-app></main-app>",
    data: {
        config: ""
    }
});
