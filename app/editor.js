import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import VueCkeditor from "vue-ckeditor5";
import Multiselect from "vue-multiselect";

const options = {
    editors: {
        classic: ClassicEditor,
    },
    name: "ckeditor"
};

window.Vue.use(VueCkeditor.plugin, options);
window.Vue.component("multiselect", Multiselect);
