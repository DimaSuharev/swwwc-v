export default {
    install(Vue, store) {
        Vue.prototype.$rights = {
            canEdit() {
                let user = store.state.config.user;
                return user.CanEdit && user.CanEdit.data[0];
            }
        };
    }
};
