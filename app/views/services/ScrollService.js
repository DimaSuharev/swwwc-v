export default {
    scrollTop(scroll) {
        let top = window.innerWidth < 1300 ? 200 : 300;
        // if (window.matchMedia("(display-mode: standalone)").matches) {
            top = 0;
        // }
        if (window.pageYOffset < 200 || scroll) {
            window.scrollTo({
                top: top,
                behavior: "smooth"
            });
        }
    },

    scrollBottom() {
        window.scrollTo({
            top: document.body.scrollHeight,
            behavior: "smooth"
        });
    }
};
