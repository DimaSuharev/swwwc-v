const state = {
    isInit: false,
    songs: [],
    events: [],
    users: [],
    churches: [],
    schedule: [],
    searchSongs: [],
    favoriteSongs: {},
    lastActivity: {
        songs: [],
        events: [],
        favorite: []
    },
    config: {
        popup: {
            state: false,
            title: "",
            type: "",
            data: ""
        },
        songQuickFilter: "",
        selectedUser: "",
        newCount: 4,
        oldCount: 4,
        user: {},
        editorLoad: false
    }
};

export default state;
