const mutations = {
    SET_SONGS: (state, songs) => {
        songs.forEach((item) => {
            if (!state.favoriteSongs[item.Id]) {
                state.favoriteSongs[item.Id] = {
                    active: false
                };
            }
        });

        state.songs = songs;
    },

    SET_LAST_ACTIVITY: (state, lastActivity) => {
        state.lastActivity = lastActivity;
    },

    SET_FAVORITE_SONGS: (state, favoriteSongs) => {
        if (favoriteSongs.length > 0) {
            favoriteSongs.forEach((item) => {
                if (state.favoriteSongs[item.SongId]) {
                    state.favoriteSongs[item.SongId].active = true;
                } else {
                    state.favoriteSongs[item.SongId] = {
                        active: true
                    };
                }
            });
        } else {
            for (let item in state.favoriteSongs) {
                state.favoriteSongs[item].active = false;
            }
        }
    },

    SET_CURRENT_USER: (state, user) => {
        state.config.user = user;
    },

    SET_USERS: (state, users) => {
        state.users = users;
    }
};

export default mutations;
