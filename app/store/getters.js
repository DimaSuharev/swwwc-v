/* eslint-disable no-invalid-this */
/* eslint-disable no-magic-numbers */
let findItemById = (array, id) => {
    return array.find((item) => {
        return item.Id === id;
    });
};

let isNewEvent = (item) => {
    let data = new Date();
    let day = data.getDate();
    let month = data.getMonth() + 1;
    let year = data.getFullYear();
    return new Date(item.Date).getTime() >= new Date(month + "/" + day + "/" + year).getTime();
};

let sortEvent = (a, b, direction) => {
    if (new Date(a.Date).getTime() > new Date(b.Date).getTime()) {
        return direction === "ASC" ? 1 : -1;
    } else if (new Date(a.Date).getTime() < new Date(b.Date).getTime()) {
        return direction === "ASC" ? -1 : 1;
    }

    return 0;
};

const getters = {
    GET_SONG_BY_ID: (state) => {
        return (id) => {
            return findItemById(state.songs, id);
        };
    },

    GET_EVENT_BY_ID: (state) => {
        return (id) => {
            return findItemById(state.events, id);
        };
    },

    GET_USER_BY_ID: (state) => {
        return (id) => {
            return findItemById(state.users, id);
        };
    },

    GET_CHURCH_BY_ID: (state) => {
        return (id) => {
            return findItemById(state.churches, id);
        };
    },

    GET_SEARCH_RESULT: (state) => {
        return state.searchSongs;
    },

    GET_ACTIVE_EVENTS: (state) => {
        return state.events.filter((item) => {
            return isNewEvent(item);
        }).sort((a, b) => {
            return sortEvent(a, b, "ASC");
        });
    },

    GET_EVENTS_ARHIVE: (state) => {
        return state.events.filter((item) => {
            return !isNewEvent(item);
        }).sort((a, b) => {
            return sortEvent(a, b, "DESC");
        });
    }
};

export default getters;
