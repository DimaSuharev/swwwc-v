import axios from "axios";

const BASE_PATH = "/";
const BASE_HEADERS = {
    headers: { "X-Requested-With": "XMLHttpRequest" }
};

const actions = {
    DATA_SERVISE: ({}, data) => {
        return axios.post(`${BASE_PATH}DataService`, data, BASE_HEADERS).catch((err) => {
           console.log(err);
        });
    },

    GET_LAST_ACTIVITY: ({ commit }) => {
        return axios.post(`${BASE_PATH}LastActivity`, null, BASE_HEADERS).then((response) => {
            if (response.data) {
                commit("SET_LAST_ACTIVITY", response.data);
                commit("SET_FAVORITE_SONGS", response.data.favorite || []);
            }

            return Promise.resolve(response);
        });
    },

    ADD_FAVORITE_SONG: ({ state }, songId) => {
        return axios.post(`${BASE_PATH}FavoriteSong`, {
            SongId: songId,
            UserId: state.config.user.Id
        }, BASE_HEADERS)
            .then((response) => {
                if (response.data !== "") {
                    state.favoriteSongs[songId].active = response.data;
                }
            })
            .catch((err) => {
                console.log(err);
            });
    },

    LOGIN: ({ getters, commit }, data) => {
        return axios.post(`${BASE_PATH}Login`, data, BASE_HEADERS)
            .then((response) => {
                if (response.data) {
                    let user = getters.GET_USER_BY_ID(response.data.Id);
                    user.favorite = response.data.favorite;
                    commit("SET_CURRENT_USER", user);
                    commit("SET_FAVORITE_SONGS", user.favorite || []);
                }

                return Promise.resolve(response);
            });
    },

    EDIT_USER: ({}, data) => {
        return axios.post(`${BASE_PATH}UserEdit`, data, BASE_HEADERS);
    },

    SEARCH_SONG_BY_NAME: ({ state }, data) => {
        return axios.post(`${BASE_PATH}SearchSongByName`, data, BASE_HEADERS)
            .then((response) => {
                if (response.data) {
                    state.searchSongs = response.data;
                }
            });
    },

    GET_SCHEDULE: () => {
        return axios.post(`${BASE_PATH}Schedule`, {}, BASE_HEADERS);
    }
};

export default actions;
