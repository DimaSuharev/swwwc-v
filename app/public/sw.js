const version = "0.0.11";
const CACHE_NAME = `my-site-cache-v${version}`;
const urlsToCache = [
    "/",
    // "/fonts/svg-icons.eot",
    // "/fonts/svg-icons.svg",
    // "/fonts/svg-icons.ttf",
    // "/fonts/svg-icons.woff",
    "/images/ico-192.png",
    "/images/ico-512.png",
    // "/js/app.bundles.min.js",
    // "/js/editor.bundles.min.js",
    "/favicon.ico"
];

self.addEventListener("install", (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log("Opened cache");
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener("activate", (event) => {

    let cacheWhitelist = [CACHE_NAME];

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});

// self.addEventListener("activate", event => {
//     event.waitUntil(self.clients.claim());
// });

self.addEventListener("fetch", (event) => {
    event.respondWith(
        caches.open(CACHE_NAME)
            .then((cache) => {
                return cache.match(event.request, {ignoreSearch: true});
            })
            .then((response) => {
                return response || fetch(event.request);
            })
    );
});
