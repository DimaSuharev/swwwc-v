import Vue from "vue";
import Router from "vue-router";
import MainSection from "@selfApp/views/modules/MainSection";
import EventSection from "@selfApp/views/modules/EventSection";
import SongsSection from "@selfApp/views/modules/SongsSection";
import ScheduleSection from "@selfApp/views/modules/ScheduleSection";
import ChurchSection from "@selfApp/views/modules/ChurchSection";
import ChurchPage from "@selfApp/views/modules/ChurchPage";
import AdminSection from "@selfApp/views/modules/AdminSection";
import SongPage from "@selfApp/views/modules/SongPage";
import EventPage from "@selfApp/views/modules/EventPage";
import ProfilePage from "@selfApp/views/modules/ProfilePage";
import AdminPageList from "@selfApp/views/modules/AdminPageList";
import BaseAdmin from "@selfApp/views/modules/BaseAdmin";

import ChurchPageAbout from "@selfApp/views/components/ChurchPageAbout";
import ChurchPageEvents from "@selfApp/views/components/ChurchPageEvents";
import ChurchPageMembers from "@selfApp/views/components/ChurchPageMembers";

Vue.use(Router);

function scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
        return savedPosition;
    }
    return { x: 0, y: 0 };
}

export default new Router({
    mode: "history",
    scrollBehavior,
    routes: [
        {
            path: "/",
            name: "Main_section",
            component: MainSection,
            meta: {}
        },
        {
            path: "/ScheduleSection",
            name: "Schedule_section",
            component: ScheduleSection,
            meta: {}
        },
        {
            path: "/EventSection",
            name: "Event_section",
            component: EventSection,
            meta: {}
        },
        {
            path: "/SongsSection",
            name: "Song_section",
            component: SongsSection,
            meta: {}
        },
        {
            path: "/Churches",
            name: "Churches_section",
            component: ChurchSection,
        },
        {
            path: "/Churche/:id",
            name: "Church_Page",
            component: ChurchPage,
            props: true,
            children: [
                {
                    path: "",
                    component: ChurchPageAbout,
                    name: "Church_Page_About",
                    props: true
                },
                {
                    path: "events",
                    component: ChurchPageEvents,
                    name: "Church_Page_Events",
                    props: true
                },
                {
                    path: "members",
                    component: ChurchPageMembers,
                    name: "Church_Page_Members",
                    props: true
                }
            ]
        },
        {
            path: "/AdminSection",
            name: "Admin_section",
            component: BaseAdmin,
            children: [
                {
                    path: "",
                    component: AdminSection,
                    name: "Admin_section_main",
                    meta: {
                        isAuth: true
                    }
                },
                {
                    path: ":type",
                    component: AdminPageList,
                    name: "Admin_page_list",
                    meta: {
                        isAuth: true
                    },
                    props: true
                }
            ],
            meta: {
                isAuth: true
            }
        },
        {
            path: "/SongPage/:id",
            name: "Song_page",
            component: SongPage,
            meta: {},
            props: true
        },
        {
            path: "/SongPage/:id/:eventId",
            name: "Song_page_in_event",
            component: SongPage,
            meta: {},
            props: true
        },
        {
            path: "/EventPage/:id",
            name: "Event_page",
            component: EventPage,
            meta: {},
            props: true
        },
        {
            path: "/ProfilePage/:id",
            name: "Profile_page",
            component: ProfilePage,
            meta: {},
            props: true
        }
    ]
});
