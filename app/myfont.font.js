module.exports = {
    files: [ "./svg/*.svg" ],
    fontName: "svg-icons",
    classPrefix: "icon-",
    baseSelector: ".icon",
    types: [ "eot", "woff", "woff2", "ttf", "svg" ],
    fixedWidth: true,
    fileName: "svg-icons.[ext]"
};
