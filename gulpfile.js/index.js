let gulp = require("gulp");
let gulpCopy = require("gulp-copy");
let streamQueue = require("streamqueue");

gulp.task("copy:ws", () => {
    let sourceFiles = [ "web/assets/*.*" ];
    return streamQueue({ objectMode: true }, gulp.src(sourceFiles))
        .pipe(gulpCopy("../../web/assets/vue/", { prefix: 100 }));
});

gulp.task("copy:images", () => {
    let sourceFiles = [ "web/assets/images/*.*" ];
    return streamQueue({ objectMode: true }, gulp.src(sourceFiles))
        .pipe(gulpCopy("../../web/assets/vue/images", { prefix: 100 }));
});
gulp.task("copy:js", () => {
    let sourceFiles = [ "web/assets/js/*.js" ];
    return streamQueue({ objectMode: true }, gulp.src(sourceFiles))
        .pipe(gulpCopy("../../web/assets/vue/js", { prefix: 100 }));
});

let copy = gulp.series("copy:ws", "copy:js", "copy:images");

gulp.task("copy", copy);
