let path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
let UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    context: __dirname,
    stats: {
        warnings: false
    },
    entry: {
        app: "./app/index.js",
        editor: "./app/editor.js"
    },
    output: {
        path: path.resolve(__dirname, "../../web", "assets/vue"),
        filename: "js/[name].js"
    },
    resolve: {
        extensions: [".js", ".json", ".vue"],
        alias: {
            "@selfApp": path.resolve(__dirname, "app/"),
            "vue": "vue/dist/vue.min"
        }
    },
    devtool: "eval",
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'app.font.css'
        }),
        new UglifyJSPlugin({
            uglifyOptions: {
                output: {
                    comments: false
                }
            }
        }),
        new VueLoaderPlugin(),
        new CopyWebpackPlugin([{ from: "app/public", to: "./" }]),
    ],

    module: {
        rules: [
            {
                test: /\.font\.js/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "webfonts-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    "sass-loader",
                    {
                        loader: "sass-resources-loader",
                        options: {
                            resources: [
                                path.resolve(__dirname, "./node_modules/coriolan-ui/mixins/_media.scss"),
                                path.resolve(__dirname, "./node_modules/coriolan-ui/tools/_variables.scss")
                            ]
                        },
                    }
                ]
            },
            {
                test: /\.vue$/,
                loader: "vue-loader"
            },
            {
                test: /\.pug$/,
                oneOf: [
                    {
                        resourceQuery: /^\?vue/,
                        use: ["pug-plain-loader"]
                    },
                    {
                        use: ["raw-loader", "pug-plain-loader"]
                    }
                ]
            }
        ]
    }
};
