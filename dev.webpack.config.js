let path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");

module.exports = {
    context: __dirname,
    stats: {
        warnings: false
    },
    entry: {
        app: "./app/index.js",
        editor: "./app/editor.js"
    },
    output: {
        path: path.resolve(__dirname, "web", "assets"),
        publicPath: "/",
        filename: "js/[name].js"
    },
    resolve: {
        extensions: [".js", ".json", ".vue"],
        alias: {
            "@selfApp": path.resolve(__dirname, "app/"),
            "vue": "vue/dist/vue.min"
        }
    },
    devServer: {
        clientLogLevel: 'warning',
        historyApiFallback: true,
        hot: true,
        contentBase: "/",
        compress: true,
        host: "localhost",
        port: 2103,
        overlay: { warnings: false, errors: true },
        publicPath: "/",
        proxy: {},
        quiet: true,
        watchOptions: {
            poll: false,
        }
    },
    mode: "development",
    devtool: "eval",
    plugins: [
        new CopyWebpackPlugin([{ from: "app/public", to: "./" }]),
        new MiniCssExtractPlugin({
            filename: "font.[contenthash].css"
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
        // new webpack.NoEmitOnErrorsPlugin(),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: "./index.html",
            inject: true
        }),
    ],

    module: {
        rules: [
            {
                test: /\.font\.js/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "webfonts-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    "sass-loader",
                    {
                        loader: "sass-resources-loader",
                        options: {
                            resources: [
                                path.resolve(__dirname, "./node_modules/coriolan-ui/mixins/_media.scss"),
                                path.resolve(__dirname, "./node_modules/coriolan-ui/tools/_variables.scss")
                            ]
                        },
                    }
                ]
            },
            {
                test: /\.vue$/,
                loader: "vue-loader"
            },
            {
                test: /\.pug$/,
                oneOf: [
                    {
                        resourceQuery: /^\?vue/,
                        use: ["pug-plain-loader"]
                    },
                    {
                        use: ["raw-loader", "pug-plain-loader"]
                    }
                ]
            }
        ]
    }
};
